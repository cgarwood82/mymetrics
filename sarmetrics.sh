#!/bin/bash
# Instead of standing up a fluentd server, I've instead decided
# just write logging that is formatted for influxdb. This
# avoids having to mess around with rsyslog as well.

# Global Variables
log=/tmp/log
influxdb_ip=192.168.100.218
influxdb_port=8086
influx_db=metrics

function cpumetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log | awk '{print "sarcpu,host=" $1 ",cpumetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function memmetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -r | awk '{print "sarmemory,host=" $1 ",memmetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function swapmetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -S | awk '{print "sarswap,host=" $1 ",swapmetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function diskmetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -dp | awk '{print "sardisk,host=" $1 ",disk=" $6 ",diskmetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function diskspacemetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -F | awk '{print "sardiskspace,host=" $1 ",disk=" $6 ",diskspacemetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function networkmetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -n DEV | awk '{print "sarnetwork,host=" $1 ",interface=" $6 ",networkmetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function networkerrormetrics () {
  # Store formatted output as a variable for easy use
  processed=$(sadf -p $log -- -n EDEV | awk '{print "sarneterror,host=" $1 ",interface=" $6 ",neterrormetric=" $7 " value="$8}')

  curl -s -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

while true
do
  # Generate sysstat data
  sar -A 1 1 -o $log > /dev/null

  # Run metric collection functions
  cpumetrics 
  memmetrics &
  swapmetrics &
  diskmetrics &
  diskspacemetrics &
  networkmetrics &
  networkerrormetrics
  
  # Remove log file 
  rm $log
done
