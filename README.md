# Agentless Metrics with InfluxDB

The goal of this project is to create a scalable, simple, solution to monitor a system only using gnu/linux tools that we would expect to be a vanilla server. 

# Installation

**This assumes you have a running influxdb server running somewhere.**

Create the database for our metrics:

	curl -XPOST "http://IP_ADDRESS:8086/query?pretty=true" --data-urlencode "q=create database metrics"

Edit the global variables in the top port of the either metrics script:

```
log=/tmp/log
influxdb_ip=192.168.100.218	
influxdb_port=8086
influx_db=metrics
```

Perform the following on an host to be monitored

```
# cp gnumetrics.sh /usr/local/bin
# cp metrics.service /etc/systemctl/system/
# systemctl enable metrics.service
# systemctl start metrics.service
```

## Security Concerns
* This assumes your api call isn't using TLS. Change the curl command if you are using encrypted communication to influx
* The database isn't protected with authorization and is considered insecure. 

# Overview of Architecture

I valued simplicity over complexity and avoided using a monolothic approach to monitoring. I ruled out tools such as nagios, zabbix, and icinga. These are client based approaches with options for agentless collection such as SSH and SNMP. Each of these solutions requires back end databases, agent configuration, and web frontends. Since we can't use a third party tool, it just makes sense to write the collector ourselves...for simplicity. 

Additionally, I've also avoided using a log aggregator. Since I'm crafting the message, I can write it directly into time series store. Again simplicity in the architecture. 

Instead, I opted for standing up an influxdb. I figured it has the simplest api to start lobbing time series data using curl, can scale for HA, easily secured with TLS, and know for it's speed. More so, since influxdb uses key pairs for indexing, sending messages in a url string is easy to read, scalable, and awesome. Long live key pairs.  

For monitoring and graphic, I'm going leveraging graphana as as it's awesome for graphing  time series data, alerting, and generally easy to build sophisticated dashboards. 

## Provided Files

This repo serves up 3 files:

```
metrics.service		# systemd unit file
sarmetrics.sh		# leverages sysstat for metrics
gnumetrics.sh		# leverages gnu/linux tools for metrics
```

Each of the scripts performs the following monitoring:  

* cpu metrics
* mem metrics
* swap space metrics
* disk metrics(io)
* disk space metrics
* network metrics

### Sample data shipped to influxdb

Output of the networkmetrics function:

```
netmetrics,host=transpute,interface=lo,metric=rxbits value=9501048
netmetrics,host=transpute,interface=lo,metric=rxpackets value=21081
netmetrics,host=transpute,interface=lo,metric=rxdrop value=0
netmetrics,host=transpute,interface=lo,metric=rxerror value=0
netmetrics,host=transpute,interface=lo,metric=txbits value=9501048
netmetrics,host=transpute,interface=lo,metric=txpackets value=21081
netmetrics,host=transpute,interface=lo,metric=txdrop value=0
netmetrics,host=transpute,interface=lo,metric=txerror value=0
netmetrics,host=transpute,interface=enp31s0,metric=rxbits value=1535696898
netmetrics,host=transpute,interface=enp31s0,metric=rxpackets value=4786473
netmetrics,host=transpute,interface=enp31s0,metric=rxdrop value=0
netmetrics,host=transpute,interface=enp31s0,metric=rxerror value=0
netmetrics,host=transpute,interface=enp31s0,metric=txbits value=585097784
netmetrics,host=transpute,interface=enp31s0,metric=txpackets value=4403210
netmetrics,host=transpute,interface=enp31s0,metric=txdrop value=0
netmetrics,host=transpute,interface=enp31s0,metric=txerror value=0

```

### sarmetrics.sh

**requires sysstate >= 10.1.6**

Due to the formatting possibilities of sysstats and the ability to capture it using gnu tools, there is considerably more metric points to collect. With that said, sarmetrics.sh is ideal but it may violate the 3rd party rule as it isn't installed by default. 

### gnumetrics.sh

**requires bc package**

gnumetrics.sh, outside of a bc (for calculating floating point numbers), is purely written leveraging bash, sed, and awk. I leverage the `/proc` fs for 

# Final Thoughts...

When clients are prohibited from installation, collecting metrics can be difficult. The the proposed solution, deploying the script and related service file is simple with configuration management. The resulting service file makes management of the scripted service simple. Finally, creating more functions and expanding on the current level of monitoring is simple as it is a bash function that is added to the while loop. 

Service validations can be created as key value boolean checks using bash operators. Configuration management could manage "templates" for application types that are generated during provisioning. So my solution may lack some of polish an agent provides or perhaps even SSH or SNMP, but provides a simple to understand and extendable solution that requires little effort to deploy. 

