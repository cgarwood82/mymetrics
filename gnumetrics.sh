#!/bin/bash
# The goal here is to use only native tools to collect metrics
# from a linux box. I plan on leveraging only the /proc fs and
# using sed and awk to format strings for influxdb. 

# Global Variables
log=/tmp/log
influxdb_ip=192.168.100.218
influxdb_port=8086
influx_db=metrics

function cpumetrics () {
  # Store some variables so we can determine a percentage of utilization...
  # Performing the calculation like so allows to determine an accurate 
  # percentage returned as an float. Anyting above 1.0 > 100$. 
  PROCS=$(grep -c processor /proc/cpuinfo)
  SYSLOAD=$(awk '{ print $1 }' /proc/loadavg)
  UTILIZED=$(bc -l <<< "scale=2; $SYSLOAD / $PROCS")

  # Store formatted output as a variable for easy use
  processed=$(echo "cpu,host=$HOSTNAME value=$UTILIZED")

  curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function memmetrics () {
  # Store some variables so we can determine what amount of RAM is available...
  # Performing this calculation generates an accurate view of memory usage 
  # since it isn't calculating for kernel caching. 
  AVAILABLE=$(cat /proc/meminfo | grep MemAvailable| awk '{ print $2 }')
  TOTAL=$(cat /proc/meminfo | grep MemTotal | awk '{ print $2 }')
  INUSE=$(bc -l <<< "scale=4; $AVAILABLE / $TOTAL")

  # Store formatted output as a variable for easy use
  processed=$(echo "memory,host=$HOSTNAME value=$INUSE")

  curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function swapmetrics () {
  # Store some variables so we can determine what amount of swap available...
  AVAILABLE=$(cat /proc/meminfo | grep SwapFree| awk '{ print $2 }')
  TOTAL=$(cat /proc/meminfo | grep SwapTotal | awk '{ print $2 }')

  # Some boxes won't have swap, this sets our metric to 100%
  if [ $TOTAL == "0" ]; then
    INUSE="1"
  else
    INUSE=$(bc -l <<< "scale=4; $AVAILABLE / $TOTAL")
  fi
  
  # Store formatted output as a variable for easy use
  processed=$(echo "swap,host=$HOSTNAME value=$INUSE")

  curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function diskmetrics () {
  # Store formatted output as a variable for easy use
  processed=$(cat /proc/diskstats | sed "s/^/$HOSTNAME /"| awk '$0{ print "diskstats,host=" $1 ",disk=" $4 " value=" $13}')

  curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function diskspacemetrics () {
  # Store formatted output as a variable for easy use
  processed=$(df| grep "^/dev.*" |sed "s/^/$HOSTNAME /g" | sed s/%// |awk '$0{ print "diskcap,host=" $1 ",disk=" $2 " value=" $6}')

  curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
}

function networkmetrics () {
  # Assign checks values to checks
  checks[2]=rxbits
  checks[3]=rxpackets
  checks[4]=rxdrop
  checks[5]=rxerror
  checks[10]=txbits
  checks[11]=txpackets
  checks[12]=txdrop
  checks[13]=txerror

  # I hate string manipulation in bash. This block generates command
  # command lines by using the array above to reference columns in
  # output from the /proc/net/dev file. I assigned the column value
  # as the key in the array and the metric type as a column name. 
  # From there, we generate our influxdb query...but there is a 
  # subcommand that actually grabs our metric using awk. AWFUL.
  cat /proc/net/dev|sed '1,2d'|sed 's/:.*//g'|sed 's/^[ \t]*//g' |
    while IFS= read -r LINE
    do
      for i in "${!checks[@]}"
      do
	processed=$(echo "netmetrics,host=$HOSTNAME,interface=$LINE,metric=${checks[$i]} value=$(echo $(grep $LINE /proc/net/dev) | awk -v var=$i '{ print $var }')")
  	curl -v -XPOST "http://$influxdb_ip:$influxdb_port/write?db=$influx_db" -d "$processed"
      done
    done
}

while true
do
  # Run metric collection functions
  cpumetrics &
  memmetrics &
  swapmetrics &
  diskmetrics &
  diskspacemetrics &
  networkmetrics 
  
  sleep 2
done
